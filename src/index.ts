import bodyParser from 'body-parser';
import express from 'express';
import { getAllPosts } from './persistence';

const PORT = 3000;

/**
 * Создаем Express-сервер
 */
export const app = express();

/**
 * Это мидлвара, которая позволяет читать BODY, который передали как JSON-объект
 */
app.use(bodyParser.json());

/**
 * Пример одного эдпоинта, используйте его как образец
 */
app.get('/posts', async (_, res) => {
  const posts = await getAllPosts();

  res.json(posts);
});

/**
 * Чтобы во время тестов ваше запущенное приложение не конфликтовало
 *  с тем, которое поднимается для тестов
 */
if (process.env.NODE_ENV !== 'test') {
  /**
   * Обратите внимание что делает этот метод -- мы подписываемся на весь трафик
   *  с порта 3000, "слушаем его"
   */
  app.listen(PORT, () => {
    console.log(`Express application started on http://localhost:${PORT}`);
  });
}
